SELECT GEN.GEN_DESCRIPCION AS GENERO_DESCRIPCION, PEL.PEL_DESCRIPCION AS PELICULA_DESCRIPCION
FROM GENEROS GEN
INNER JOIN PELICULAS PEL ON PEL.GEN_ID = GEN.GEN_ID;


SELECT PEL.PEL_DESCRIPCION AS PELICULA_DESCRIPCION, ACT.ACT_NOMBRE, ACT.ACT_APELLIDO
FROM PELICULAS PEL
INNER JOIN PELICULAS_ACTORES PA ON PA.PEL_ID = PEL.PEL_ID
INNER JOIN ACTORES ACT ON ACT.ACT_ID = PA.ACT_ID;
