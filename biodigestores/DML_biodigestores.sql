/*

INSERT INTO biodigestores  VALUES
('Anakin','S34°39"12.38"','O58°37 11.1"'), 
('Obi Wan','S34°39"15.22"','O58°37"13.1"'),
('Darth Vader','S34°39"10.38"','O58°37"09.3"');

*/

INSERT INTO biodigestores (BIO_NOMBRE,BIO_LATITUD,BIO_LONGITUD)  VALUES
('Anakin','S34°39"12.38"','O58°37 11.1"'), 
('Obi Wan','S34°39"15.22"','O58°37"13.1"'),
('Darth Vader','S34°39"10.38"','O58°37"09.3"'); 

/*

INSERT INTO tiposDeSensores VALUES
('Temperatura','Grados Centigrados'), 
('Humedad','%'),
('Presion','PSI');

*/

INSERT INTO tiposDeSensores (TIP_DESCRIPCION,TIP_UNIDAD_MEDIDA) VALUES
('Temperatura','Grados Centigrados'), 
('Humedad','%'),
('Presion','PSI');

/*

INSERT INTO sensores VALUES
(1,1,'IST','AG OUT'), 
(1,2,'HUMIDITY','H1101'),
(1,3,'HONEYWELL','MLH'),
(2,2,'HUMIDITY','H1101'),
(2,3,'HONEYWELL','MLH'),
(3,1,'IST','AG OUT');

*/

INSERT INTO sensores (BIO_ID,TIP_ID,SEN_MARCA,SEN_MODELO) VALUES
(1,1,'IST','AG OUT'), 
(1,2,'HUMIDITY','H1101'),
(1,3,'HONEYWELL','MLH'),
(2,2,'HUMIDITY','H1101'),
(2,3,'HONEYWELL','MLH'),
(3,1,'IST','AG OUT');

/*

INSERT INTO personas VALUES
('Juan','PEREZ','13/2/1985'),
('Gabriel','CASAS','17/11/1972'),
('Susana','ROMERO','13/5/1990');

*/

INSERT INTO personas (PER_NOMBRE,PER_APELLIDO,PER_FECHA) VALUES
('Juan','PEREZ','13/2/1985'),
('Gabriel','CASAS','17/11/1972'),
('Susana','ROMERO','13/5/1990');

/*

INSERT INTO personas_biodigestores VALUES
(1,1),
(1,2),
(1,3),
(2,1),
(2,3),
(3,3);

*/

INSERT INTO personas_biodigestores (PER_ID,BIO_ID) VALUES
(1,1),
(1,2),
(1,3),
(2,1),
(2,3),
(3,3);

/*

INSERT INTO mediciones VALUES
(1,12.3,'11/10/2022  08:00:00'),
(1,12.5,'11/10/2022  08:05:00'),
(1,12.8,'11/10/2022  08:10:00'),
(1,13.1,'11/10/2022  08:15:00'),
(1,13.3,'11/10/2022  08:20:00'),
(1,12.0,'11/10/2022  08:25:00'),
(2,33.5,'11/10/2022  08:00:00'),
(2,43.2,'11/10/2022  08:05:00'),
(2,45.8,'11/10/2022  08:10:00'),
(2,50.2,'11/10/2022  08:15:00'),
(2,48.1,'11/10/2022  08:20:00'),
(2,49.7,'11/10/2022  08:25:00');

*/

INSERT INTO mediciones (SEN_ID,MED_VALOR,MED_FECHA) VALUES
(1,12.3,'11/10/2022  08:00:00'),
(1,12.5,'11/10/2022  08:05:00'),
(1,12.8,'11/10/2022  08:10:00'),
(1,13.1,'11/10/2022  08:15:00'),
(1,13.3,'11/10/2022  08:20:00'),
(1,12.0,'11/10/2022  08:25:00'),
(2,33.5,'11/10/2022  08:00:00'),
(2,43.2,'11/10/2022  08:05:00'),
(2,45.8,'11/10/2022  08:10:00'),
(2,50.2,'11/10/2022  08:15:00'),
(2,48.1,'11/10/2022  08:20:00'),
(2,49.7,'11/10/2022  08:25:00');