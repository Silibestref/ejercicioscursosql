USE master;

/*  BACKUP */
BACKUP DATABASE bio_silibestref
	TO DISK ='D:\SQL\ejercicioscursosql\biodigestores\backup\bio_silibestref_20221014_1524.bak'
	WITH INIT, COMPRESSION;

/* RESTORE */
RESTORE DATABASE bio_silibestref
  FROM DISK = 'D:\SQL\ejercicioscursosql\biodigestores\backup\bio_silibestref_20221014_1524.bak'
  WITH REPLACE;


/* VIZUALIZAR DETALLES DE LA TABLA / TIPO BACKUP */
  
SELECT NAME, database_id, recovery_model_desc AS model
FROM sys.databases
WHERE NAME ='bio_silibestref';

SELECT DATABASEPROPERTYEX('master', 'recovery');

SELECT NAME, database_id, recovery_model_desc AS model
FROM sys.databases
WHERE NAME ='master'