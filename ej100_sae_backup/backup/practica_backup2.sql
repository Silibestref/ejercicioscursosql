RESTORE DATABASE sae_silibestref
  FROM DISK = 'D:\SQL\ejercicioscursosql\ej100_sae_backup\backup\sae_silibestref_20221017_1135.bak'
  WITH REPLACE;


INSERT INTO alumnos(PART_ID,COM_ID,ALU_NOMBRE,ALU_APELLIDO,ALU_TELEFONO,ALU_DNI,ALU_EMAIL) VALUES 
 (100,5,'Federico','Silibestre','su casa',NULL,NULL);


BACKUP DATABASE sae_silibestref
	TO DISK ='D:\SQL\ejercicioscursosql\ej100_sae_backup\backup\sae_silibestref_diferencial.dif'
	WITH INIT, DIFFERENTIAL, COMPRESSION;


BACKUP LOG sae_silibestref
	TO DISK ='D:\SQL\ejercicioscursosql\ej100_sae_backup\backup\sae_silibestref_log.log'
	WITH INIT, COMPRESSION;    

BACKUP DATABASE sae_silibestref
	TO DISK ='D:\SQL\ejercicioscursosql\ej100_sae_backup\backup\sae_silibestref_sin_compresion.bak'
	WITH INIT;	



/*  BACKUP DIFERENCIAL */

/* PASO 1 HACEMOS UN RESTORE DE NUESTRO ULTIMO FULL BACKUP EL CUAL QUEDA EN STANDBY HASTA CARGUEMOS EL LOG DIFERENCIAL Y LE DEMOS RECOVERY */

RESTORE DATABASE sae_silibestref
	FROM DISK = 'D:\SQL\ejercicioscursosql\ej100_sae_backup\backup\sae_silibestref_20221017_1135.bak'
	WITH REPLACE, NORECOVERY;
GO	

/* PASO 2 HACEMOS UN RESTORE DE NUESTRO LOG DIFERENCIAL */

RESTORE DATABASE sae_silibestref
	FROM DISK = 'D:\SQL\ejercicioscursosql\ej100_sae_backup\backup\sae_silibestref_diferencial.dif'
	WITH RECOVERY;
GO	