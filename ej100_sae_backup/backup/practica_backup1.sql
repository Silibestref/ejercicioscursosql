/*  BACKUP */

BACKUP DATABASE sae_silibestref
	TO DISK ='D:\SQL\ejercicioscursosql\ej100_sae_backup\backup\sae_silibestref_20221017_1135.bak'
	WITH INIT, COMPRESSION;

/*  RESTORE */

RESTORE DATABASE sae_silibestref
  FROM DISK = 'D:\SQL\ejercicioscursosql\ej100_sae_backup\backup\sae_silibestref_20221017_1135.bak'
  WITH REPLACE;

/* VIZUALIZAR DETALLES DE LA TABLA / TIPO BACKUP */

select name, database_id, recovery_model_desc as model
from sys.databases
where name ='sae_silibestref'

select DATABASEPROPERTYEX('master', 'recovery')

select name, database_id, recovery_model_desc as model
from sys.databases
where name ='master'

